# trunarrative_client.TruRestApi

All URIs are relative to *https://localhost/TruAPI/rest/TruRest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_batch_run**](TruRestApi.md#create_batch_run) | **GET** /CreateBatchRun | 
[**find_address**](TruRestApi.md#find_address) | **GET** /FindAddress | 
[**get_document_image_results**](TruRestApi.md#get_document_image_results) | **GET** /GetDocumentImageResults | 
[**get_journey_response**](TruRestApi.md#get_journey_response) | **GET** /GetJourneyResponse | 
[**get_journey_status**](TruRestApi.md#get_journey_status) | **GET** /GetJourneyStatus | 
[**get_journeys**](TruRestApi.md#get_journeys) | **GET** /GetJourneys | 
[**get_status**](TruRestApi.md#get_status) | **GET** /GetStatus | 
[**run_journey**](TruRestApi.md#run_journey) | **POST** /RunJourney | 
[**run_ruleset**](TruRestApi.md#run_ruleset) | **POST** /RunRuleset | 


# **create_batch_run**
> CreateBatchRunResponse create_batch_run()





### Example
```python
from __future__ import print_function
import time
import trunarrative_client
from trunarrative_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = trunarrative_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = trunarrative_client.TruRestApi(trunarrative_client.ApiClient(configuration))

try:
    api_response = api_instance.create_batch_run()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TruRestApi->create_batch_run: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**CreateBatchRunResponse**](CreateBatchRunResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **find_address**
> list[AddressItem] find_address(post_code)





### Example
```python
from __future__ import print_function
import time
import trunarrative_client
from trunarrative_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = trunarrative_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = trunarrative_client.TruRestApi(trunarrative_client.ApiClient(configuration))
post_code = '' # str |  (default to )

try:
    api_response = api_instance.find_address(post_code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TruRestApi->find_address: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **post_code** | **str**|  | [default to ]

### Return type

[**list[AddressItem]**](AddressItem.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_document_image_results**
> TruResultResponse get_document_image_results(tru_id)





### Example
```python
from __future__ import print_function
import time
import trunarrative_client
from trunarrative_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = trunarrative_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = trunarrative_client.TruRestApi(trunarrative_client.ApiClient(configuration))
tru_id = '' # str |  (default to )

try:
    api_response = api_instance.get_document_image_results(tru_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TruRestApi->get_document_image_results: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tru_id** | **str**|  | [default to ]

### Return type

[**TruResultResponse**](TruResultResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_journey_response**
> list[Transaction] get_journey_response(uid=uid, client_application_reference=client_application_reference)





### Example
```python
from __future__ import print_function
import time
import trunarrative_client
from trunarrative_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = trunarrative_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = trunarrative_client.TruRestApi(trunarrative_client.ApiClient(configuration))
uid = '' # str |  (optional) (default to )
client_application_reference = '' # str |  (optional) (default to )

try:
    api_response = api_instance.get_journey_response(uid=uid, client_application_reference=client_application_reference)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TruRestApi->get_journey_response: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **str**|  | [optional] [default to ]
 **client_application_reference** | **str**|  | [optional] [default to ]

### Return type

[**list[Transaction]**](Transaction.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_journey_status**
> JourneyStatus get_journey_status(journey_id)





### Example
```python
from __future__ import print_function
import time
import trunarrative_client
from trunarrative_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = trunarrative_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = trunarrative_client.TruRestApi(trunarrative_client.ApiClient(configuration))
journey_id = '' # str |  (default to )

try:
    api_response = api_instance.get_journey_status(journey_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TruRestApi->get_journey_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **journey_id** | **str**|  | [default to ]

### Return type

[**JourneyStatus**](JourneyStatus.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_journeys**
> list[JourneyData] get_journeys()





### Example
```python
from __future__ import print_function
import time
import trunarrative_client
from trunarrative_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = trunarrative_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = trunarrative_client.TruRestApi(trunarrative_client.ApiClient(configuration))

try:
    api_response = api_instance.get_journeys()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TruRestApi->get_journeys: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[JourneyData]**](JourneyData.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_status**
> SystemStatus get_status()





### Example
```python
from __future__ import print_function
import time
import trunarrative_client
from trunarrative_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = trunarrative_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = trunarrative_client.TruRestApi(trunarrative_client.ApiClient(configuration))

try:
    api_response = api_instance.get_status()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TruRestApi->get_status: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SystemStatus**](SystemStatus.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **run_journey**
> Transaction run_journey(request)





### Example
```python
from __future__ import print_function
import time
import trunarrative_client
from trunarrative_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = trunarrative_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = trunarrative_client.TruRestApi(trunarrative_client.ApiClient(configuration))
request = trunarrative_client.ApplicationcustomerReferencegoodsAccountbatchRunIdjourneyIDcompanypersonRecord() # ApplicationcustomerReferencegoodsAccountbatchRunIdjourneyIDcompanypersonRecord | Request

try:
    api_response = api_instance.run_journey(request)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TruRestApi->run_journey: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**ApplicationcustomerReferencegoodsAccountbatchRunIdjourneyIDcompanypersonRecord**](ApplicationcustomerReferencegoodsAccountbatchRunIdjourneyIDcompanypersonRecord.md)| Request | 

### Return type

[**Transaction**](Transaction.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **run_ruleset**
> RunRulesetResponse run_ruleset(request)





### Example
```python
from __future__ import print_function
import time
import trunarrative_client
from trunarrative_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basic
configuration = trunarrative_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = trunarrative_client.TruRestApi(trunarrative_client.ApiClient(configuration))
request = trunarrative_client.RulesetIdrunIDRecord() # RulesetIdrunIDRecord | 

try:
    api_response = api_instance.run_ruleset(request)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TruRestApi->run_ruleset: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request** | [**RulesetIdrunIDRecord**](RulesetIdrunIDRecord.md)|  | 

### Return type

[**RunRulesetResponse**](RunRulesetResponse.md)

### Authorization

[basic](../README.md#basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

