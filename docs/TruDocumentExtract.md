# TruDocumentExtract

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **str** |  | [optional] [default to '']
**middle_name** | **str** |  | [optional] [default to '']
**last_name** | **str** |  | [optional] [default to '']
**gender** | **str** |  | [optional] [default to '']
**date_of_birth** | **str** |  | [optional] [default to '']
**birthplace** | **str** |  | [optional] [default to '']
**issuing_country** | **str** |  | [optional] [default to '']
**nationality** | **str** |  | [optional] [default to '']
**country_iso3** | **str** |  | [optional] [default to '']
**document_type** | **str** |  | [optional] [default to '']
**document_version** | **str** |  | [optional] [default to '']
**document_number** | **str** |  | [optional] [default to '']
**expiry_date** | **str** |  | [optional] [default to '']
**date_of_issue** | **str** |  | [optional] [default to '']
**address** | **str** |  | [optional] [default to '']
**verified_address** | [**TruAddress**](TruAddress.md) |  | [optional] 
**mrz** | **str** |  | [optional] [default to '']
**is_document_expired** | **bool** |  | [optional] 
**social_security_number** | **str** |  | [optional] [default to '']
**processed_document_features** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


