# GoodsOrderItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_reference** | **str** |  | [optional] [default to '']
**product_type** | **str** |  | [optional] [default to '']
**product_name** | **str** |  | [optional] [default to '']
**price** | **float** |  | [optional] 
**quantity** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


