# GoodsAccount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | **str** |  | [optional] [default to '']
**number_of_orders_to_date** | **int** |  | [optional] 
**current_account_balance** | **float** |  | [optional] 
**current_circulating_order_value** | **float** |  | [optional] 
**credit_limit** | **float** |  | [optional] 
**value_of_payments_to_date** | **float** |  | [optional] 
**number_of_payments_to_date** | **float** |  | [optional] 
**active_direct_debit_to_date** | **bool** |  | [optional] 
**value_of_returns_to_date** | **float** |  | [optional] 
**value_of_adjustments_to_date** | **float** |  | [optional] 
**type_of_account** | **str** |  | [optional] [default to '']
**confirmed_at_address** | **str** |  | [optional] [default to '']
**customer_trading_status** | **str** |  | [optional] [default to '']
**delivery_postcode_area** | **str** |  | [optional] [default to '']
**days_since_account_opened** | **int** |  | [optional] 
**scorecard** | **str** |  | [optional] [default to '']
**credit_score** | **int** |  | [optional] 
**account_application_method** | **str** |  | [optional] [default to '']
**website** | **str** |  | [optional] [default to '']
**days_since_last_order** | **int** |  | [optional] 
**days_since_last_payment** | **int** |  | [optional] 
**value_of_ordered_items_to_date** | **float** |  | [optional] 
**gone_away_indicator** | **bool** |  | [optional] 
**current_credit_limit_utilisation** | **int** |  | [optional] 
**requested_credit_limit_utilisation** | **int** |  | [optional] 
**number_of_payment_cards6_months** | **int** |  | [optional] 
**dist_between_billing_and_shipping_address** | **int** |  | [optional] 
**dist_between_billing_and_shipping_address_units** | **str** |  | [optional] [default to '']
**goods_order** | [**list[GoodsOrder]**](GoodsOrder.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


