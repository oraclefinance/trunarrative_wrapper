# CompanyAddressItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address_line1** | **str** |  | [optional] [default to '']
**address_line2** | **str** |  | [optional] [default to '']
**address_line3** | **str** |  | [optional] [default to '']
**address_line4** | **str** |  | [optional] [default to '']
**address_line5** | **str** |  | [optional] [default to '']
**address_line6** | **str** |  | [optional] [default to '']
**address_line7** | **str** |  | [optional] [default to '']
**address_line8** | **str** |  | [optional] [default to '']
**zip_postcode** | **str** |  | [optional] [default to '']
**country** | **str** |  | [optional] [default to '']
**moving_in_date** | **date** |  | [optional] 
**moving_out_date** | **date** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


