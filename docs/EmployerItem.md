# EmployerItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company_name** | **str** |  | [optional] [default to '']
**company_phone_number** | **str** |  | [optional] [default to '']
**employment_start_date** | **date** |  | [optional] 
**employment_end_date** | **date** |  | [optional] 
**employer_address** | [**EmployerAddressItem**](EmployerAddressItem.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


