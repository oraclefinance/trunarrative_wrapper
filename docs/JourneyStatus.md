# JourneyStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**label** | **str** |  | [default to '']
**order** | **int** |  | 
**is_active** | **bool** |  | 
**is_decision** | **bool** | Is a decision | [optional] 
**is_assignment** | **bool** | Can be used in an assignment | [optional] 
**external_journey_status_id** | **int** | The corresponding external status | 
**description** | **str** |  | [default to '']
**code** | **str** | The unique code for this status | [default to '']
**is_editable** | **bool** |  | [optional] 
**is_system** | **bool** | Is assigned by the system | [optional] 
**is_refer_assignment** | **bool** |  | [optional] 
**action** | **str** |  | [optional] [default to '']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


