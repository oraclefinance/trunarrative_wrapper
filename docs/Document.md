# Document

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gbr** | [**GBR**](GBR.md) |  | [optional] 
**idn** | [**IDN**](IDN.md) |  | [optional] 
**usa** | [**USA**](USA.md) |  | [optional] 
**international** | [**International**](International.md) |  | [optional] 
**mys** | [**MYS**](MYS.md) |  | [optional] 
**images** | [**Images**](Images.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


