# TransactionRulesetOutcome

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**score** | **int** | The total ruleset score | [optional] 
**risk_level** | **str** |  | [optional] [default to '']
**status** | **str** |  | [optional] [default to '']
**ruleset_name** | **str** |  | [optional] [default to '']
**rule_outcomes** | [**list[TransactionRuleOutcome]**](TransactionRuleOutcome.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


