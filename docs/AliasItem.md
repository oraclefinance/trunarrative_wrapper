# AliasItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** |  | [optional] [default to '']
**first_name** | **str** |  | [optional] [default to '']
**middle_names** | **str** |  | [optional] [default to '']
**last_name** | **str** |  | [optional] [default to '']
**gender** | **str** |  | [optional] [default to '']
**effective_date** | **date** |  | [optional] 
**end_date** | **date** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


