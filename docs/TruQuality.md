# TruQuality

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_acceptable_blur_mrz** | **bool** |  | [optional] 
**is_acceptable_blur_viz** | **bool** |  | [optional] 
**is_acceptable_dark** | **bool** |  | [optional] 
**is_acceptable_dpi** | **bool** |  | [optional] 
**is_acceptable_entropy** | **bool** |  | [optional] 
**is_acceptable_layout** | **bool** |  | [optional] 
**is_acceptable_mrz** | **bool** |  | [optional] 
**is_acceptable_obliquity** | **bool** |  | [optional] 
**is_acceptable_reflection_mrz** | **bool** |  | [optional] 
**is_acceptable_reflection_viz** | **bool** |  | [optional] 
**is_acceptable_size** | **bool** |  | [optional] 
**is_acceptable_text_confidence_mrz** | **bool** |  | [optional] 
**is_acceptable_text_confidence_viz** | **bool** |  | [optional] 
**is_generally_acceptable** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


