# RulesetIdrunIDRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ruleset_id** | **int** |  | [optional] 
**run_id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


