# DrivingLicence

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**part1_surname** | **str** |  | [optional] [default to '']
**part2_given_names** | **str** |  | [optional] [default to '']
**part3_dob** | **date** |  | [optional] 
**part4a_start_date** | **date** |  | [optional] 
**part4b_expiry_date** | **date** |  | [optional] 
**part4c_issue_authority** | **str** |  | [optional] [default to '']
**part5_number** | **str** |  | [optional] [default to '']
**part5_issue_number** | **str** |  | [optional] [default to '']
**part8_address_line** | **str** |  | [optional] [default to '']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


