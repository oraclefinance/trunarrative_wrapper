# MYS

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identity_card** | [**list[IdentityCarditem]**](IdentityCarditem.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


