# Application

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application_date_time** | **datetime** |  | [optional] 
**client_application_reference** | **str** |  | [optional] [default to '']
**device_id** | **str** |  | [optional] [default to '']
**ip_address** | **str** |  | [optional] [default to '']
**amount_or_limit** | **float** |  | [optional] 
**deposit** | **float** |  | [optional] 
**repayment** | **float** |  | [optional] 
**term** | **int** |  | [optional] 
**country** | **str** |  | [optional] [default to '']
**capture_date_time** | **datetime** |  | [optional] 
**client_decision** | **str** |  | [optional] [default to '']
**client_decision_reason** | **str** |  | [optional] [default to '']
**client_decision_date_time** | **datetime** |  | [optional] 
**client_application_purpose** | **str** |  | [optional] [default to '']
**process_code** | **str** |  | [optional] [default to '']
**process_reason** | **str** |  | [optional] [default to '']
**relationship_end_date** | **date** |  | [optional] 
**motor_details** | [**MotorDetail**](MotorDetail.md) |  | [optional] 
**client_user_id** | **str** |  | [optional] [default to '']
**client_organisation_id** | **str** |  | [optional] [default to '']
**application_decision** | **str** |  | [optional] [default to '']
**application_decision_reason** | **str** |  | [optional] [default to '']
**champion_challenger_number** | **int** |  | [optional] 
**black_box** | **str** |  | [optional] [default to '']
**funding_countries** | [**list[FundingCountryItem]**](FundingCountryItem.md) |  | [optional] 
**source_of_deposit** | **str** |  | [optional] [default to '']
**third_party_token** | [**list[ThirdPartyTokens]**](ThirdPartyTokens.md) |  | [optional] 
**trading_currencies** | [**list[TradingCurrencyItem]**](TradingCurrencyItem.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


