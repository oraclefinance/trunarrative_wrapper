# JourneyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | [**LinkedItem**](LinkedItem.md) |  | 
**journey_type** | [**LinkedItem**](LinkedItem.md) |  | 
**name** | **str** |  | [default to '']
**description** | **str** |  | [optional] [default to '']
**is_parrallel** | **bool** |  | [optional] 
**obsolete_rule_set** | [**LinkedItem**](LinkedItem.md) |  | [optional] 
**obsolete_descision_matrix** | [**LinkedItem**](LinkedItem.md) |  | [optional] 
**score_card** | [**LinkedItem**](LinkedItem.md) |  | [optional] 
**created_on** | **datetime** |  | [optional] 
**created_by** | [**LinkedItem**](LinkedItem.md) |  | [optional] 
**reference** | **str** |  | [optional] [default to '']
**is_draft** | **bool** |  | [optional] 
**id** | **int** |  | 
**icon** | **str** |  | [optional] [default to '']
**app_ids** | **list[int]** |  | [optional] 
**organisation_id** | **int** |  | 
**sub_category_id** | **int** | the journey sub category based on artefacts | [optional] 
**matrix_ids** | **list[int]** |  | [optional] 
**ruleset_ids** | **list[int]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


