# Transaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**uid** | **str** |  | [optional] [default to '']
**journey_id** | **int** |  | [optional] 
**journey_name** | **str** |  | [optional] [default to '']
**organisation_id** | **int** |  | [optional] 
**user_id** | **int** |  | [optional] 
**performed_by** | **str** | The name of the user this transaction was carried out by | [optional] [default to '']
**customer_reference** | **str** |  | [optional] [default to '']
**name_reference** | **str** |  | [optional] [default to '']
**schema_version** | **float** |  | [optional] 
**tru_id** | **str** |  | [optional] [default to '']
**time_to_execute_rule_set** | **str** |  | [optional] 
**total_rule_score** | **int** |  | [optional] 
**risk_level** | **str** |  | [optional] [default to '']
**error** | **str** |  | [optional] [default to '']
**request_type** | **str** |  | [optional] [default to '']
**request_ip_address** | **str** |  | [optional] [default to '']
**channel** | **str** |  | [optional] [default to '']
**journey_type** | [**LinkedItem**](LinkedItem.md) |  | [optional] 
**start_date_time** | **datetime** |  | [optional] 
**end_date_time** | **datetime** |  | [optional] 
**country** | [**LinkedItem**](LinkedItem.md) |  | [optional] 
**app_outomes** | [**list[TransactionAppOutcome]**](TransactionAppOutcome.md) |  | [optional] 
**rule_outcomes** | [**list[TransactionRuleOutcome]**](TransactionRuleOutcome.md) |  | [optional] 
**response_id** | **int** |  | [optional] 
**status** | [**JourneyStatus**](JourneyStatus.md) |  | [optional] 
**journey_reference** | **str** |  | [optional] [default to '']
**reference** | **str** |  | [optional] [default to '']
**scorecard_rule_outcomes** | [**list[TransactionRuleOutcome]**](TransactionRuleOutcome.md) |  | [optional] 
**redirect_url** | **str** |  | [optional] [default to '']
**ruleset_outcomes** | [**list[TransactionRulesetOutcome]**](TransactionRulesetOutcome.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


