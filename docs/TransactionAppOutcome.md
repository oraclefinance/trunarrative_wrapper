# TransactionAppOutcome

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**app_id** | **int** |  | [optional] 
**app_name** | **str** |  | [optional] [default to '']
**outcome** | **int** |  | 
**number_of_matches** | **int** |  | [optional] 
**entity_id** | **str** |  | [optional] [default to '']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


