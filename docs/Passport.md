# Passport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | **str** |  | [optional] [default to '']
**number** | **str** |  | [optional] [default to '']
**surname** | **str** |  | [optional] [default to '']
**given_names** | **str** |  | [optional] [default to '']
**nationality** | **str** |  | [optional] [default to '']
**date_of_birth** | **date** |  | [optional] 
**place_of_birth** | **str** |  | [optional] [default to '']
**place_of_issue** | **str** |  | [optional] [default to '']
**issuing_authority** | **str** |  | [optional] [default to '']
**issue_date** | **date** |  | [optional] 
**expiry_date** | **date** |  | [optional] 
**mrz_line1** | **str** |  | [optional] [default to '']
**mrz_line2** | **str** |  | [optional] [default to '']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


