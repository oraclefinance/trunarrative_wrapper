# TransactionRuleOutcome

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**outcome** | **bool** |  | [optional] 
**score** | **int** |  | [optional] 
**client_reference** | **int** |  | [optional] 
**description** | **str** |  | [optional] [default to '']
**name** | **str** |  | [optional] [default to '']
**version_number** | **int** |  | [optional] 
**revision_number** | **int** |  | [optional] 
**uid** | **str** |  | [optional] [default to '']
**reference** | **str** |  | [optional] [default to '']
**reference_type_id** | **int** |  | [optional] 
**number_of_match_records** | **int** |  | [optional] 
**app_ids** | **str** |  | [optional] [default to '']
**apps** | [**list[TransactionAppOutcome]**](TransactionAppOutcome.md) | List of apps (Output only) | [optional] 
**risk_severity_id** | **int** |  | [optional] 
**sub_category_id** | **int** |  | [optional] 
**ruleset_id** | **int** |  | [optional] 
**ruleset_name** | **str** |  | [optional] [default to '']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


