# TruProcessedImage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**quality** | [**TruQuality**](TruQuality.md) |  | [optional] 
**quality_before_adjustment** | [**TruQuality**](TruQuality.md) |  | [optional] 
**is_coloured** | **bool** |  | [optional] 
**is_reduced** | **bool** |  | [optional] 
**is_valid_size** | **bool** |  | [optional] 
**cropped_height** | **int** |  | [optional] 
**cropped_width** | **int** |  | [optional] 
**original_height** | **int** |  | [optional] 
**original_width** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


