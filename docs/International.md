# International

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**passport** | [**Passport**](Passport.md) |  | [optional] 
**identity_card** | [**IdentityCarditem**](IdentityCarditem.md) |  | [optional] 
**tax_number** | [**TaxNumber**](TaxNumber.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


