# AddressItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**address_line1** | **str** |  | [optional] [default to '']
**address_line2** | **str** |  | [optional] [default to '']
**address_line3** | **str** |  | [optional] [default to '']
**address_line4** | **str** |  | [optional] [default to '']
**locality** | **str** |  | [optional] [default to '']
**city** | **str** |  | [optional] [default to '']
**county** | **str** |  | [optional] [default to '']
**full_address** | **str** |  | [optional] [default to '']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


