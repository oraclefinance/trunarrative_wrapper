# TruDocument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_data_extracted** | **bool** |  | [optional] 
**overall_quality** | **str** |  | [optional] [default to '']
**document_extract** | [**TruDocumentExtract**](TruDocumentExtract.md) |  | [optional] 
**processed_images** | [**list[TruProcessedImage]**](TruProcessedImage.md) |  | [optional] 
**forgery_tests** | [**list[TruForgeryTest]**](TruForgeryTest.md) |  | [optional] 
**cropped_photo** | [**TruCroppedPhoto**](TruCroppedPhoto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


