# TruCroppedPhoto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**photo_id** | **str** |  | [optional] [default to '']
**photo_type** | **str** |  | [optional] [default to '']
**height** | **int** |  | [optional] 
**width** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


