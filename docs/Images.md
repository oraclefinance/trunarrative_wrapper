# Images

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timeout_seconds** | **int** |  | [optional] 
**face** | [**FaceImage**](FaceImage.md) |  | [optional] 
**documents** | [**list[DocumentImage]**](DocumentImage.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


