# ApplicationcustomerReferencegoodsAccountbatchRunIdjourneyIDcompanypersonRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application** | [**Application**](Application.md) |  | [optional] 
**customer_reference** | **str** |  | [optional] [default to '']
**goods_account** | [**list[GoodsAccount]**](GoodsAccount.md) |  | [optional] 
**batch_run_id** | **int** |  | [optional] 
**journey_id** | **int** |  | [optional] 
**company** | [**list[CompanyItem]**](CompanyItem.md) |  | [optional] 
**person** | [**list[PersonItem]**](PersonItem.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


