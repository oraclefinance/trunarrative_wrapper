# MotorDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicle_reg_number** | **str** |  | [optional] [default to '']
**engine_number** | **str** |  | [optional] [default to '']
**chassis_number** | **str** |  | [optional] [default to '']
**make** | **str** |  | [optional] [default to '']
**model** | **str** |  | [optional] [default to '']
**purchase_price** | **int** |  | [optional] 
**new_or_used** | **str** |  | [optional] [default to '']
**year_of_manufacture** | **str** |  | [optional] [default to '']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


