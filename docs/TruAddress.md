# TruAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**administrative_area_level1** | **str** |  | [optional] [default to '']
**administrative_area_level2** | **str** |  | [optional] [default to '']
**country** | **str** |  | [optional] [default to '']
**locality** | **str** |  | [optional] [default to '']
**postal_code** | **str** |  | [optional] [default to '']
**postal_code_prefix** | **str** |  | [optional] [default to '']
**postal_code_suffix** | **str** |  | [optional] [default to '']
**route** | **str** |  | [optional] [default to '']
**street_number** | **str** |  | [optional] [default to '']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


