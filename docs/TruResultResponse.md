# TruResultResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference_id** | **str** |  | [optional] [default to '']
**processing_id** | **str** |  | [optional] [default to '']
**authenticity** | **str** |  | [optional] [default to '']
**processing_result** | **str** |  | [optional] [default to '']
**completion_status** | **str** |  | [optional] [default to '']
**face** | [**TruFace**](TruFace.md) |  | [optional] 
**documents** | [**list[TruDocument]**](TruDocument.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


