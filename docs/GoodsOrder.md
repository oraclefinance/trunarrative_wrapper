# GoodsOrder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order_number** | **str** |  | [optional] [default to '']
**order_date_time** | **datetime** |  | [optional] 
**total_order_amount** | **float** |  | [optional] 
**order_currency** | **str** |  | [optional] [default to '']
**trading_brand** | **str** |  | [optional] [default to '']
**trading_website** | **str** |  | [optional] [default to '']
**delivery_type** | **str** |  | [optional] [default to '']
**delivery_speed** | **str** |  | [optional] [default to '']
**initial_order_indicator** | **bool** |  | [optional] 
**credit_line_or_cash** | **str** |  | [optional] [default to '']
**order_method** | **str** |  | [optional] [default to '']
**contact_telephone** | **str** |  | [optional] [default to '']
**avs_check_result** | **str** |  | [optional] [default to '']
**secure3d_result** | **str** |  | [optional] [default to '']
**cv2_response** | **str** |  | [optional] [default to '']
**postcode_response** | **str** |  | [optional] [default to '']
**address_avs_response** | **str** |  | [optional] [default to '']
**billing_cust_age_on_order_date** | **int** |  | [optional] 
**ip_address** | **str** |  | [optional] [default to '']
**goods_order_item** | [**list[GoodsOrderItem]**](GoodsOrderItem.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


