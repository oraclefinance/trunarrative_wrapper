# coding: utf-8

"""
    TruRest

    ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import trunarrative_client
from trunarrative_client.models.employer_item import EmployerItem  # noqa: E501
from trunarrative_client.rest import ApiException


class TestEmployerItem(unittest.TestCase):
    """EmployerItem unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testEmployerItem(self):
        """Test EmployerItem"""
        # FIXME: construct object with mandatory attributes with example values
        # model = trunarrative_client.models.employer_item.EmployerItem()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
