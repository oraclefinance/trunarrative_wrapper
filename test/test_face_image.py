# coding: utf-8

"""
    TruRest

    ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import trunarrative_client
from trunarrative_client.models.face_image import FaceImage  # noqa: E501
from trunarrative_client.rest import ApiException


class TestFaceImage(unittest.TestCase):
    """FaceImage unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testFaceImage(self):
        """Test FaceImage"""
        # FIXME: construct object with mandatory attributes with example values
        # model = trunarrative_client.models.face_image.FaceImage()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
