# coding: utf-8

"""
    TruRest

    ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import trunarrative_client
from trunarrative_client.models.create_batch_run_response import CreateBatchRunResponse  # noqa: E501
from trunarrative_client.rest import ApiException


class TestCreateBatchRunResponse(unittest.TestCase):
    """CreateBatchRunResponse unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testCreateBatchRunResponse(self):
        """Test CreateBatchRunResponse"""
        # FIXME: construct object with mandatory attributes with example values
        # model = trunarrative_client.models.create_batch_run_response.CreateBatchRunResponse()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
