# coding: utf-8

"""
    TruRest

    ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from trunarrative_client.models.tru_address import TruAddress  # noqa: F401,E501


class TruDocumentExtract(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'first_name': 'str',
        'middle_name': 'str',
        'last_name': 'str',
        'gender': 'str',
        'date_of_birth': 'str',
        'birthplace': 'str',
        'issuing_country': 'str',
        'nationality': 'str',
        'country_iso3': 'str',
        'document_type': 'str',
        'document_version': 'str',
        'document_number': 'str',
        'expiry_date': 'str',
        'date_of_issue': 'str',
        'address': 'str',
        'verified_address': 'TruAddress',
        'mrz': 'str',
        'is_document_expired': 'bool',
        'social_security_number': 'str',
        'processed_document_features': 'list[str]'
    }

    attribute_map = {
        'first_name': 'FirstName',
        'middle_name': 'MiddleName',
        'last_name': 'LastName',
        'gender': 'Gender',
        'date_of_birth': 'DateOfBirth',
        'birthplace': 'Birthplace',
        'issuing_country': 'IssuingCountry',
        'nationality': 'Nationality',
        'country_iso3': 'CountryISO3',
        'document_type': 'DocumentType',
        'document_version': 'DocumentVersion',
        'document_number': 'DocumentNumber',
        'expiry_date': 'ExpiryDate',
        'date_of_issue': 'DateOfIssue',
        'address': 'Address',
        'verified_address': 'VerifiedAddress',
        'mrz': 'Mrz',
        'is_document_expired': 'IsDocumentExpired',
        'social_security_number': 'SocialSecurityNumber',
        'processed_document_features': 'ProcessedDocumentFeatures'
    }

    def __init__(self, first_name='', middle_name='', last_name='', gender='', date_of_birth='', birthplace='', issuing_country='', nationality='', country_iso3='', document_type='', document_version='', document_number='', expiry_date='', date_of_issue='', address='', verified_address=None, mrz='', is_document_expired=None, social_security_number='', processed_document_features=None):  # noqa: E501
        """TruDocumentExtract - a model defined in Swagger"""  # noqa: E501

        self._first_name = None
        self._middle_name = None
        self._last_name = None
        self._gender = None
        self._date_of_birth = None
        self._birthplace = None
        self._issuing_country = None
        self._nationality = None
        self._country_iso3 = None
        self._document_type = None
        self._document_version = None
        self._document_number = None
        self._expiry_date = None
        self._date_of_issue = None
        self._address = None
        self._verified_address = None
        self._mrz = None
        self._is_document_expired = None
        self._social_security_number = None
        self._processed_document_features = None
        self.discriminator = None

        if first_name is not None:
            self.first_name = first_name
        if middle_name is not None:
            self.middle_name = middle_name
        if last_name is not None:
            self.last_name = last_name
        if gender is not None:
            self.gender = gender
        if date_of_birth is not None:
            self.date_of_birth = date_of_birth
        if birthplace is not None:
            self.birthplace = birthplace
        if issuing_country is not None:
            self.issuing_country = issuing_country
        if nationality is not None:
            self.nationality = nationality
        if country_iso3 is not None:
            self.country_iso3 = country_iso3
        if document_type is not None:
            self.document_type = document_type
        if document_version is not None:
            self.document_version = document_version
        if document_number is not None:
            self.document_number = document_number
        if expiry_date is not None:
            self.expiry_date = expiry_date
        if date_of_issue is not None:
            self.date_of_issue = date_of_issue
        if address is not None:
            self.address = address
        if verified_address is not None:
            self.verified_address = verified_address
        if mrz is not None:
            self.mrz = mrz
        if is_document_expired is not None:
            self.is_document_expired = is_document_expired
        if social_security_number is not None:
            self.social_security_number = social_security_number
        if processed_document_features is not None:
            self.processed_document_features = processed_document_features

    @property
    def first_name(self):
        """Gets the first_name of this TruDocumentExtract.  # noqa: E501


        :return: The first_name of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._first_name

    @first_name.setter
    def first_name(self, first_name):
        """Sets the first_name of this TruDocumentExtract.


        :param first_name: The first_name of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._first_name = first_name

    @property
    def middle_name(self):
        """Gets the middle_name of this TruDocumentExtract.  # noqa: E501


        :return: The middle_name of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._middle_name

    @middle_name.setter
    def middle_name(self, middle_name):
        """Sets the middle_name of this TruDocumentExtract.


        :param middle_name: The middle_name of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._middle_name = middle_name

    @property
    def last_name(self):
        """Gets the last_name of this TruDocumentExtract.  # noqa: E501


        :return: The last_name of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._last_name

    @last_name.setter
    def last_name(self, last_name):
        """Sets the last_name of this TruDocumentExtract.


        :param last_name: The last_name of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._last_name = last_name

    @property
    def gender(self):
        """Gets the gender of this TruDocumentExtract.  # noqa: E501


        :return: The gender of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._gender

    @gender.setter
    def gender(self, gender):
        """Sets the gender of this TruDocumentExtract.


        :param gender: The gender of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._gender = gender

    @property
    def date_of_birth(self):
        """Gets the date_of_birth of this TruDocumentExtract.  # noqa: E501


        :return: The date_of_birth of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._date_of_birth

    @date_of_birth.setter
    def date_of_birth(self, date_of_birth):
        """Sets the date_of_birth of this TruDocumentExtract.


        :param date_of_birth: The date_of_birth of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._date_of_birth = date_of_birth

    @property
    def birthplace(self):
        """Gets the birthplace of this TruDocumentExtract.  # noqa: E501


        :return: The birthplace of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._birthplace

    @birthplace.setter
    def birthplace(self, birthplace):
        """Sets the birthplace of this TruDocumentExtract.


        :param birthplace: The birthplace of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._birthplace = birthplace

    @property
    def issuing_country(self):
        """Gets the issuing_country of this TruDocumentExtract.  # noqa: E501


        :return: The issuing_country of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._issuing_country

    @issuing_country.setter
    def issuing_country(self, issuing_country):
        """Sets the issuing_country of this TruDocumentExtract.


        :param issuing_country: The issuing_country of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._issuing_country = issuing_country

    @property
    def nationality(self):
        """Gets the nationality of this TruDocumentExtract.  # noqa: E501


        :return: The nationality of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._nationality

    @nationality.setter
    def nationality(self, nationality):
        """Sets the nationality of this TruDocumentExtract.


        :param nationality: The nationality of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._nationality = nationality

    @property
    def country_iso3(self):
        """Gets the country_iso3 of this TruDocumentExtract.  # noqa: E501


        :return: The country_iso3 of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._country_iso3

    @country_iso3.setter
    def country_iso3(self, country_iso3):
        """Sets the country_iso3 of this TruDocumentExtract.


        :param country_iso3: The country_iso3 of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._country_iso3 = country_iso3

    @property
    def document_type(self):
        """Gets the document_type of this TruDocumentExtract.  # noqa: E501


        :return: The document_type of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._document_type

    @document_type.setter
    def document_type(self, document_type):
        """Sets the document_type of this TruDocumentExtract.


        :param document_type: The document_type of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._document_type = document_type

    @property
    def document_version(self):
        """Gets the document_version of this TruDocumentExtract.  # noqa: E501


        :return: The document_version of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._document_version

    @document_version.setter
    def document_version(self, document_version):
        """Sets the document_version of this TruDocumentExtract.


        :param document_version: The document_version of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._document_version = document_version

    @property
    def document_number(self):
        """Gets the document_number of this TruDocumentExtract.  # noqa: E501


        :return: The document_number of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._document_number

    @document_number.setter
    def document_number(self, document_number):
        """Sets the document_number of this TruDocumentExtract.


        :param document_number: The document_number of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._document_number = document_number

    @property
    def expiry_date(self):
        """Gets the expiry_date of this TruDocumentExtract.  # noqa: E501


        :return: The expiry_date of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._expiry_date

    @expiry_date.setter
    def expiry_date(self, expiry_date):
        """Sets the expiry_date of this TruDocumentExtract.


        :param expiry_date: The expiry_date of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._expiry_date = expiry_date

    @property
    def date_of_issue(self):
        """Gets the date_of_issue of this TruDocumentExtract.  # noqa: E501


        :return: The date_of_issue of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._date_of_issue

    @date_of_issue.setter
    def date_of_issue(self, date_of_issue):
        """Sets the date_of_issue of this TruDocumentExtract.


        :param date_of_issue: The date_of_issue of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._date_of_issue = date_of_issue

    @property
    def address(self):
        """Gets the address of this TruDocumentExtract.  # noqa: E501


        :return: The address of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._address

    @address.setter
    def address(self, address):
        """Sets the address of this TruDocumentExtract.


        :param address: The address of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._address = address

    @property
    def verified_address(self):
        """Gets the verified_address of this TruDocumentExtract.  # noqa: E501


        :return: The verified_address of this TruDocumentExtract.  # noqa: E501
        :rtype: TruAddress
        """
        return self._verified_address

    @verified_address.setter
    def verified_address(self, verified_address):
        """Sets the verified_address of this TruDocumentExtract.


        :param verified_address: The verified_address of this TruDocumentExtract.  # noqa: E501
        :type: TruAddress
        """

        self._verified_address = verified_address

    @property
    def mrz(self):
        """Gets the mrz of this TruDocumentExtract.  # noqa: E501


        :return: The mrz of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._mrz

    @mrz.setter
    def mrz(self, mrz):
        """Sets the mrz of this TruDocumentExtract.


        :param mrz: The mrz of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._mrz = mrz

    @property
    def is_document_expired(self):
        """Gets the is_document_expired of this TruDocumentExtract.  # noqa: E501


        :return: The is_document_expired of this TruDocumentExtract.  # noqa: E501
        :rtype: bool
        """
        return self._is_document_expired

    @is_document_expired.setter
    def is_document_expired(self, is_document_expired):
        """Sets the is_document_expired of this TruDocumentExtract.


        :param is_document_expired: The is_document_expired of this TruDocumentExtract.  # noqa: E501
        :type: bool
        """

        self._is_document_expired = is_document_expired

    @property
    def social_security_number(self):
        """Gets the social_security_number of this TruDocumentExtract.  # noqa: E501


        :return: The social_security_number of this TruDocumentExtract.  # noqa: E501
        :rtype: str
        """
        return self._social_security_number

    @social_security_number.setter
    def social_security_number(self, social_security_number):
        """Sets the social_security_number of this TruDocumentExtract.


        :param social_security_number: The social_security_number of this TruDocumentExtract.  # noqa: E501
        :type: str
        """

        self._social_security_number = social_security_number

    @property
    def processed_document_features(self):
        """Gets the processed_document_features of this TruDocumentExtract.  # noqa: E501


        :return: The processed_document_features of this TruDocumentExtract.  # noqa: E501
        :rtype: list[str]
        """
        return self._processed_document_features

    @processed_document_features.setter
    def processed_document_features(self, processed_document_features):
        """Sets the processed_document_features of this TruDocumentExtract.


        :param processed_document_features: The processed_document_features of this TruDocumentExtract.  # noqa: E501
        :type: list[str]
        """

        self._processed_document_features = processed_document_features

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, TruDocumentExtract):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
