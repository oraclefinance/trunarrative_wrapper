# coding: utf-8

"""
    TruRest

    ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from trunarrative_client.models.goods_order import GoodsOrder  # noqa: F401,E501


class GoodsAccount(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'account_number': 'str',
        'number_of_orders_to_date': 'int',
        'current_account_balance': 'float',
        'current_circulating_order_value': 'float',
        'credit_limit': 'float',
        'value_of_payments_to_date': 'float',
        'number_of_payments_to_date': 'float',
        'active_direct_debit_to_date': 'bool',
        'value_of_returns_to_date': 'float',
        'value_of_adjustments_to_date': 'float',
        'type_of_account': 'str',
        'confirmed_at_address': 'str',
        'customer_trading_status': 'str',
        'delivery_postcode_area': 'str',
        'days_since_account_opened': 'int',
        'scorecard': 'str',
        'credit_score': 'int',
        'account_application_method': 'str',
        'website': 'str',
        'days_since_last_order': 'int',
        'days_since_last_payment': 'int',
        'value_of_ordered_items_to_date': 'float',
        'gone_away_indicator': 'bool',
        'current_credit_limit_utilisation': 'int',
        'requested_credit_limit_utilisation': 'int',
        'number_of_payment_cards6_months': 'int',
        'dist_between_billing_and_shipping_address': 'int',
        'dist_between_billing_and_shipping_address_units': 'str',
        'goods_order': 'list[GoodsOrder]'
    }

    attribute_map = {
        'account_number': 'accountNumber',
        'number_of_orders_to_date': 'numberOfOrdersToDate',
        'current_account_balance': 'currentAccountBalance',
        'current_circulating_order_value': 'currentCirculatingOrderValue',
        'credit_limit': 'creditLimit',
        'value_of_payments_to_date': 'valueOfPaymentsToDate',
        'number_of_payments_to_date': 'numberOfPaymentsToDate',
        'active_direct_debit_to_date': 'activeDirectDebitToDate',
        'value_of_returns_to_date': 'valueOfReturnsToDate',
        'value_of_adjustments_to_date': 'valueOfAdjustmentsToDate',
        'type_of_account': 'typeOfAccount',
        'confirmed_at_address': 'confirmedAtAddress',
        'customer_trading_status': 'customerTradingStatus',
        'delivery_postcode_area': 'deliveryPostcodeArea',
        'days_since_account_opened': 'daysSinceAccountOpened',
        'scorecard': 'scorecard',
        'credit_score': 'creditScore',
        'account_application_method': 'accountApplicationMethod',
        'website': 'website',
        'days_since_last_order': 'daysSinceLastOrder',
        'days_since_last_payment': 'daysSinceLastPayment',
        'value_of_ordered_items_to_date': 'valueOfOrderedItemsToDate',
        'gone_away_indicator': 'goneAwayIndicator',
        'current_credit_limit_utilisation': 'currentCreditLimitUtilisation',
        'requested_credit_limit_utilisation': 'requestedCreditLimitUtilisation',
        'number_of_payment_cards6_months': 'numberOfPaymentCards6Months',
        'dist_between_billing_and_shipping_address': 'distBetweenBillingAndShippingAddress',
        'dist_between_billing_and_shipping_address_units': 'distBetweenBillingAndShippingAddressUnits',
        'goods_order': 'goodsOrder'
    }

    def __init__(self, account_number='', number_of_orders_to_date=None, current_account_balance=None, current_circulating_order_value=None, credit_limit=None, value_of_payments_to_date=None, number_of_payments_to_date=None, active_direct_debit_to_date=None, value_of_returns_to_date=None, value_of_adjustments_to_date=None, type_of_account='', confirmed_at_address='', customer_trading_status='', delivery_postcode_area='', days_since_account_opened=None, scorecard='', credit_score=None, account_application_method='', website='', days_since_last_order=None, days_since_last_payment=None, value_of_ordered_items_to_date=None, gone_away_indicator=None, current_credit_limit_utilisation=None, requested_credit_limit_utilisation=None, number_of_payment_cards6_months=None, dist_between_billing_and_shipping_address=None, dist_between_billing_and_shipping_address_units='', goods_order=None):  # noqa: E501
        """GoodsAccount - a model defined in Swagger"""  # noqa: E501

        self._account_number = None
        self._number_of_orders_to_date = None
        self._current_account_balance = None
        self._current_circulating_order_value = None
        self._credit_limit = None
        self._value_of_payments_to_date = None
        self._number_of_payments_to_date = None
        self._active_direct_debit_to_date = None
        self._value_of_returns_to_date = None
        self._value_of_adjustments_to_date = None
        self._type_of_account = None
        self._confirmed_at_address = None
        self._customer_trading_status = None
        self._delivery_postcode_area = None
        self._days_since_account_opened = None
        self._scorecard = None
        self._credit_score = None
        self._account_application_method = None
        self._website = None
        self._days_since_last_order = None
        self._days_since_last_payment = None
        self._value_of_ordered_items_to_date = None
        self._gone_away_indicator = None
        self._current_credit_limit_utilisation = None
        self._requested_credit_limit_utilisation = None
        self._number_of_payment_cards6_months = None
        self._dist_between_billing_and_shipping_address = None
        self._dist_between_billing_and_shipping_address_units = None
        self._goods_order = None
        self.discriminator = None

        if account_number is not None:
            self.account_number = account_number
        if number_of_orders_to_date is not None:
            self.number_of_orders_to_date = number_of_orders_to_date
        if current_account_balance is not None:
            self.current_account_balance = current_account_balance
        if current_circulating_order_value is not None:
            self.current_circulating_order_value = current_circulating_order_value
        if credit_limit is not None:
            self.credit_limit = credit_limit
        if value_of_payments_to_date is not None:
            self.value_of_payments_to_date = value_of_payments_to_date
        if number_of_payments_to_date is not None:
            self.number_of_payments_to_date = number_of_payments_to_date
        if active_direct_debit_to_date is not None:
            self.active_direct_debit_to_date = active_direct_debit_to_date
        if value_of_returns_to_date is not None:
            self.value_of_returns_to_date = value_of_returns_to_date
        if value_of_adjustments_to_date is not None:
            self.value_of_adjustments_to_date = value_of_adjustments_to_date
        if type_of_account is not None:
            self.type_of_account = type_of_account
        if confirmed_at_address is not None:
            self.confirmed_at_address = confirmed_at_address
        if customer_trading_status is not None:
            self.customer_trading_status = customer_trading_status
        if delivery_postcode_area is not None:
            self.delivery_postcode_area = delivery_postcode_area
        if days_since_account_opened is not None:
            self.days_since_account_opened = days_since_account_opened
        if scorecard is not None:
            self.scorecard = scorecard
        if credit_score is not None:
            self.credit_score = credit_score
        if account_application_method is not None:
            self.account_application_method = account_application_method
        if website is not None:
            self.website = website
        if days_since_last_order is not None:
            self.days_since_last_order = days_since_last_order
        if days_since_last_payment is not None:
            self.days_since_last_payment = days_since_last_payment
        if value_of_ordered_items_to_date is not None:
            self.value_of_ordered_items_to_date = value_of_ordered_items_to_date
        if gone_away_indicator is not None:
            self.gone_away_indicator = gone_away_indicator
        if current_credit_limit_utilisation is not None:
            self.current_credit_limit_utilisation = current_credit_limit_utilisation
        if requested_credit_limit_utilisation is not None:
            self.requested_credit_limit_utilisation = requested_credit_limit_utilisation
        if number_of_payment_cards6_months is not None:
            self.number_of_payment_cards6_months = number_of_payment_cards6_months
        if dist_between_billing_and_shipping_address is not None:
            self.dist_between_billing_and_shipping_address = dist_between_billing_and_shipping_address
        if dist_between_billing_and_shipping_address_units is not None:
            self.dist_between_billing_and_shipping_address_units = dist_between_billing_and_shipping_address_units
        if goods_order is not None:
            self.goods_order = goods_order

    @property
    def account_number(self):
        """Gets the account_number of this GoodsAccount.  # noqa: E501


        :return: The account_number of this GoodsAccount.  # noqa: E501
        :rtype: str
        """
        return self._account_number

    @account_number.setter
    def account_number(self, account_number):
        """Sets the account_number of this GoodsAccount.


        :param account_number: The account_number of this GoodsAccount.  # noqa: E501
        :type: str
        """

        self._account_number = account_number

    @property
    def number_of_orders_to_date(self):
        """Gets the number_of_orders_to_date of this GoodsAccount.  # noqa: E501


        :return: The number_of_orders_to_date of this GoodsAccount.  # noqa: E501
        :rtype: int
        """
        return self._number_of_orders_to_date

    @number_of_orders_to_date.setter
    def number_of_orders_to_date(self, number_of_orders_to_date):
        """Sets the number_of_orders_to_date of this GoodsAccount.


        :param number_of_orders_to_date: The number_of_orders_to_date of this GoodsAccount.  # noqa: E501
        :type: int
        """

        self._number_of_orders_to_date = number_of_orders_to_date

    @property
    def current_account_balance(self):
        """Gets the current_account_balance of this GoodsAccount.  # noqa: E501


        :return: The current_account_balance of this GoodsAccount.  # noqa: E501
        :rtype: float
        """
        return self._current_account_balance

    @current_account_balance.setter
    def current_account_balance(self, current_account_balance):
        """Sets the current_account_balance of this GoodsAccount.


        :param current_account_balance: The current_account_balance of this GoodsAccount.  # noqa: E501
        :type: float
        """

        self._current_account_balance = current_account_balance

    @property
    def current_circulating_order_value(self):
        """Gets the current_circulating_order_value of this GoodsAccount.  # noqa: E501


        :return: The current_circulating_order_value of this GoodsAccount.  # noqa: E501
        :rtype: float
        """
        return self._current_circulating_order_value

    @current_circulating_order_value.setter
    def current_circulating_order_value(self, current_circulating_order_value):
        """Sets the current_circulating_order_value of this GoodsAccount.


        :param current_circulating_order_value: The current_circulating_order_value of this GoodsAccount.  # noqa: E501
        :type: float
        """

        self._current_circulating_order_value = current_circulating_order_value

    @property
    def credit_limit(self):
        """Gets the credit_limit of this GoodsAccount.  # noqa: E501


        :return: The credit_limit of this GoodsAccount.  # noqa: E501
        :rtype: float
        """
        return self._credit_limit

    @credit_limit.setter
    def credit_limit(self, credit_limit):
        """Sets the credit_limit of this GoodsAccount.


        :param credit_limit: The credit_limit of this GoodsAccount.  # noqa: E501
        :type: float
        """

        self._credit_limit = credit_limit

    @property
    def value_of_payments_to_date(self):
        """Gets the value_of_payments_to_date of this GoodsAccount.  # noqa: E501


        :return: The value_of_payments_to_date of this GoodsAccount.  # noqa: E501
        :rtype: float
        """
        return self._value_of_payments_to_date

    @value_of_payments_to_date.setter
    def value_of_payments_to_date(self, value_of_payments_to_date):
        """Sets the value_of_payments_to_date of this GoodsAccount.


        :param value_of_payments_to_date: The value_of_payments_to_date of this GoodsAccount.  # noqa: E501
        :type: float
        """

        self._value_of_payments_to_date = value_of_payments_to_date

    @property
    def number_of_payments_to_date(self):
        """Gets the number_of_payments_to_date of this GoodsAccount.  # noqa: E501


        :return: The number_of_payments_to_date of this GoodsAccount.  # noqa: E501
        :rtype: float
        """
        return self._number_of_payments_to_date

    @number_of_payments_to_date.setter
    def number_of_payments_to_date(self, number_of_payments_to_date):
        """Sets the number_of_payments_to_date of this GoodsAccount.


        :param number_of_payments_to_date: The number_of_payments_to_date of this GoodsAccount.  # noqa: E501
        :type: float
        """

        self._number_of_payments_to_date = number_of_payments_to_date

    @property
    def active_direct_debit_to_date(self):
        """Gets the active_direct_debit_to_date of this GoodsAccount.  # noqa: E501


        :return: The active_direct_debit_to_date of this GoodsAccount.  # noqa: E501
        :rtype: bool
        """
        return self._active_direct_debit_to_date

    @active_direct_debit_to_date.setter
    def active_direct_debit_to_date(self, active_direct_debit_to_date):
        """Sets the active_direct_debit_to_date of this GoodsAccount.


        :param active_direct_debit_to_date: The active_direct_debit_to_date of this GoodsAccount.  # noqa: E501
        :type: bool
        """

        self._active_direct_debit_to_date = active_direct_debit_to_date

    @property
    def value_of_returns_to_date(self):
        """Gets the value_of_returns_to_date of this GoodsAccount.  # noqa: E501


        :return: The value_of_returns_to_date of this GoodsAccount.  # noqa: E501
        :rtype: float
        """
        return self._value_of_returns_to_date

    @value_of_returns_to_date.setter
    def value_of_returns_to_date(self, value_of_returns_to_date):
        """Sets the value_of_returns_to_date of this GoodsAccount.


        :param value_of_returns_to_date: The value_of_returns_to_date of this GoodsAccount.  # noqa: E501
        :type: float
        """

        self._value_of_returns_to_date = value_of_returns_to_date

    @property
    def value_of_adjustments_to_date(self):
        """Gets the value_of_adjustments_to_date of this GoodsAccount.  # noqa: E501


        :return: The value_of_adjustments_to_date of this GoodsAccount.  # noqa: E501
        :rtype: float
        """
        return self._value_of_adjustments_to_date

    @value_of_adjustments_to_date.setter
    def value_of_adjustments_to_date(self, value_of_adjustments_to_date):
        """Sets the value_of_adjustments_to_date of this GoodsAccount.


        :param value_of_adjustments_to_date: The value_of_adjustments_to_date of this GoodsAccount.  # noqa: E501
        :type: float
        """

        self._value_of_adjustments_to_date = value_of_adjustments_to_date

    @property
    def type_of_account(self):
        """Gets the type_of_account of this GoodsAccount.  # noqa: E501


        :return: The type_of_account of this GoodsAccount.  # noqa: E501
        :rtype: str
        """
        return self._type_of_account

    @type_of_account.setter
    def type_of_account(self, type_of_account):
        """Sets the type_of_account of this GoodsAccount.


        :param type_of_account: The type_of_account of this GoodsAccount.  # noqa: E501
        :type: str
        """

        self._type_of_account = type_of_account

    @property
    def confirmed_at_address(self):
        """Gets the confirmed_at_address of this GoodsAccount.  # noqa: E501


        :return: The confirmed_at_address of this GoodsAccount.  # noqa: E501
        :rtype: str
        """
        return self._confirmed_at_address

    @confirmed_at_address.setter
    def confirmed_at_address(self, confirmed_at_address):
        """Sets the confirmed_at_address of this GoodsAccount.


        :param confirmed_at_address: The confirmed_at_address of this GoodsAccount.  # noqa: E501
        :type: str
        """

        self._confirmed_at_address = confirmed_at_address

    @property
    def customer_trading_status(self):
        """Gets the customer_trading_status of this GoodsAccount.  # noqa: E501


        :return: The customer_trading_status of this GoodsAccount.  # noqa: E501
        :rtype: str
        """
        return self._customer_trading_status

    @customer_trading_status.setter
    def customer_trading_status(self, customer_trading_status):
        """Sets the customer_trading_status of this GoodsAccount.


        :param customer_trading_status: The customer_trading_status of this GoodsAccount.  # noqa: E501
        :type: str
        """

        self._customer_trading_status = customer_trading_status

    @property
    def delivery_postcode_area(self):
        """Gets the delivery_postcode_area of this GoodsAccount.  # noqa: E501


        :return: The delivery_postcode_area of this GoodsAccount.  # noqa: E501
        :rtype: str
        """
        return self._delivery_postcode_area

    @delivery_postcode_area.setter
    def delivery_postcode_area(self, delivery_postcode_area):
        """Sets the delivery_postcode_area of this GoodsAccount.


        :param delivery_postcode_area: The delivery_postcode_area of this GoodsAccount.  # noqa: E501
        :type: str
        """

        self._delivery_postcode_area = delivery_postcode_area

    @property
    def days_since_account_opened(self):
        """Gets the days_since_account_opened of this GoodsAccount.  # noqa: E501


        :return: The days_since_account_opened of this GoodsAccount.  # noqa: E501
        :rtype: int
        """
        return self._days_since_account_opened

    @days_since_account_opened.setter
    def days_since_account_opened(self, days_since_account_opened):
        """Sets the days_since_account_opened of this GoodsAccount.


        :param days_since_account_opened: The days_since_account_opened of this GoodsAccount.  # noqa: E501
        :type: int
        """

        self._days_since_account_opened = days_since_account_opened

    @property
    def scorecard(self):
        """Gets the scorecard of this GoodsAccount.  # noqa: E501


        :return: The scorecard of this GoodsAccount.  # noqa: E501
        :rtype: str
        """
        return self._scorecard

    @scorecard.setter
    def scorecard(self, scorecard):
        """Sets the scorecard of this GoodsAccount.


        :param scorecard: The scorecard of this GoodsAccount.  # noqa: E501
        :type: str
        """

        self._scorecard = scorecard

    @property
    def credit_score(self):
        """Gets the credit_score of this GoodsAccount.  # noqa: E501


        :return: The credit_score of this GoodsAccount.  # noqa: E501
        :rtype: int
        """
        return self._credit_score

    @credit_score.setter
    def credit_score(self, credit_score):
        """Sets the credit_score of this GoodsAccount.


        :param credit_score: The credit_score of this GoodsAccount.  # noqa: E501
        :type: int
        """

        self._credit_score = credit_score

    @property
    def account_application_method(self):
        """Gets the account_application_method of this GoodsAccount.  # noqa: E501


        :return: The account_application_method of this GoodsAccount.  # noqa: E501
        :rtype: str
        """
        return self._account_application_method

    @account_application_method.setter
    def account_application_method(self, account_application_method):
        """Sets the account_application_method of this GoodsAccount.


        :param account_application_method: The account_application_method of this GoodsAccount.  # noqa: E501
        :type: str
        """

        self._account_application_method = account_application_method

    @property
    def website(self):
        """Gets the website of this GoodsAccount.  # noqa: E501


        :return: The website of this GoodsAccount.  # noqa: E501
        :rtype: str
        """
        return self._website

    @website.setter
    def website(self, website):
        """Sets the website of this GoodsAccount.


        :param website: The website of this GoodsAccount.  # noqa: E501
        :type: str
        """

        self._website = website

    @property
    def days_since_last_order(self):
        """Gets the days_since_last_order of this GoodsAccount.  # noqa: E501


        :return: The days_since_last_order of this GoodsAccount.  # noqa: E501
        :rtype: int
        """
        return self._days_since_last_order

    @days_since_last_order.setter
    def days_since_last_order(self, days_since_last_order):
        """Sets the days_since_last_order of this GoodsAccount.


        :param days_since_last_order: The days_since_last_order of this GoodsAccount.  # noqa: E501
        :type: int
        """

        self._days_since_last_order = days_since_last_order

    @property
    def days_since_last_payment(self):
        """Gets the days_since_last_payment of this GoodsAccount.  # noqa: E501


        :return: The days_since_last_payment of this GoodsAccount.  # noqa: E501
        :rtype: int
        """
        return self._days_since_last_payment

    @days_since_last_payment.setter
    def days_since_last_payment(self, days_since_last_payment):
        """Sets the days_since_last_payment of this GoodsAccount.


        :param days_since_last_payment: The days_since_last_payment of this GoodsAccount.  # noqa: E501
        :type: int
        """

        self._days_since_last_payment = days_since_last_payment

    @property
    def value_of_ordered_items_to_date(self):
        """Gets the value_of_ordered_items_to_date of this GoodsAccount.  # noqa: E501


        :return: The value_of_ordered_items_to_date of this GoodsAccount.  # noqa: E501
        :rtype: float
        """
        return self._value_of_ordered_items_to_date

    @value_of_ordered_items_to_date.setter
    def value_of_ordered_items_to_date(self, value_of_ordered_items_to_date):
        """Sets the value_of_ordered_items_to_date of this GoodsAccount.


        :param value_of_ordered_items_to_date: The value_of_ordered_items_to_date of this GoodsAccount.  # noqa: E501
        :type: float
        """

        self._value_of_ordered_items_to_date = value_of_ordered_items_to_date

    @property
    def gone_away_indicator(self):
        """Gets the gone_away_indicator of this GoodsAccount.  # noqa: E501


        :return: The gone_away_indicator of this GoodsAccount.  # noqa: E501
        :rtype: bool
        """
        return self._gone_away_indicator

    @gone_away_indicator.setter
    def gone_away_indicator(self, gone_away_indicator):
        """Sets the gone_away_indicator of this GoodsAccount.


        :param gone_away_indicator: The gone_away_indicator of this GoodsAccount.  # noqa: E501
        :type: bool
        """

        self._gone_away_indicator = gone_away_indicator

    @property
    def current_credit_limit_utilisation(self):
        """Gets the current_credit_limit_utilisation of this GoodsAccount.  # noqa: E501


        :return: The current_credit_limit_utilisation of this GoodsAccount.  # noqa: E501
        :rtype: int
        """
        return self._current_credit_limit_utilisation

    @current_credit_limit_utilisation.setter
    def current_credit_limit_utilisation(self, current_credit_limit_utilisation):
        """Sets the current_credit_limit_utilisation of this GoodsAccount.


        :param current_credit_limit_utilisation: The current_credit_limit_utilisation of this GoodsAccount.  # noqa: E501
        :type: int
        """

        self._current_credit_limit_utilisation = current_credit_limit_utilisation

    @property
    def requested_credit_limit_utilisation(self):
        """Gets the requested_credit_limit_utilisation of this GoodsAccount.  # noqa: E501


        :return: The requested_credit_limit_utilisation of this GoodsAccount.  # noqa: E501
        :rtype: int
        """
        return self._requested_credit_limit_utilisation

    @requested_credit_limit_utilisation.setter
    def requested_credit_limit_utilisation(self, requested_credit_limit_utilisation):
        """Sets the requested_credit_limit_utilisation of this GoodsAccount.


        :param requested_credit_limit_utilisation: The requested_credit_limit_utilisation of this GoodsAccount.  # noqa: E501
        :type: int
        """

        self._requested_credit_limit_utilisation = requested_credit_limit_utilisation

    @property
    def number_of_payment_cards6_months(self):
        """Gets the number_of_payment_cards6_months of this GoodsAccount.  # noqa: E501


        :return: The number_of_payment_cards6_months of this GoodsAccount.  # noqa: E501
        :rtype: int
        """
        return self._number_of_payment_cards6_months

    @number_of_payment_cards6_months.setter
    def number_of_payment_cards6_months(self, number_of_payment_cards6_months):
        """Sets the number_of_payment_cards6_months of this GoodsAccount.


        :param number_of_payment_cards6_months: The number_of_payment_cards6_months of this GoodsAccount.  # noqa: E501
        :type: int
        """

        self._number_of_payment_cards6_months = number_of_payment_cards6_months

    @property
    def dist_between_billing_and_shipping_address(self):
        """Gets the dist_between_billing_and_shipping_address of this GoodsAccount.  # noqa: E501


        :return: The dist_between_billing_and_shipping_address of this GoodsAccount.  # noqa: E501
        :rtype: int
        """
        return self._dist_between_billing_and_shipping_address

    @dist_between_billing_and_shipping_address.setter
    def dist_between_billing_and_shipping_address(self, dist_between_billing_and_shipping_address):
        """Sets the dist_between_billing_and_shipping_address of this GoodsAccount.


        :param dist_between_billing_and_shipping_address: The dist_between_billing_and_shipping_address of this GoodsAccount.  # noqa: E501
        :type: int
        """

        self._dist_between_billing_and_shipping_address = dist_between_billing_and_shipping_address

    @property
    def dist_between_billing_and_shipping_address_units(self):
        """Gets the dist_between_billing_and_shipping_address_units of this GoodsAccount.  # noqa: E501


        :return: The dist_between_billing_and_shipping_address_units of this GoodsAccount.  # noqa: E501
        :rtype: str
        """
        return self._dist_between_billing_and_shipping_address_units

    @dist_between_billing_and_shipping_address_units.setter
    def dist_between_billing_and_shipping_address_units(self, dist_between_billing_and_shipping_address_units):
        """Sets the dist_between_billing_and_shipping_address_units of this GoodsAccount.


        :param dist_between_billing_and_shipping_address_units: The dist_between_billing_and_shipping_address_units of this GoodsAccount.  # noqa: E501
        :type: str
        """

        self._dist_between_billing_and_shipping_address_units = dist_between_billing_and_shipping_address_units

    @property
    def goods_order(self):
        """Gets the goods_order of this GoodsAccount.  # noqa: E501


        :return: The goods_order of this GoodsAccount.  # noqa: E501
        :rtype: list[GoodsOrder]
        """
        return self._goods_order

    @goods_order.setter
    def goods_order(self, goods_order):
        """Sets the goods_order of this GoodsAccount.


        :param goods_order: The goods_order of this GoodsAccount.  # noqa: E501
        :type: list[GoodsOrder]
        """

        self._goods_order = goods_order

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, GoodsAccount):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
