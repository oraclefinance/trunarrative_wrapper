# coding: utf-8

"""
    TruRest

    ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class RunRulesetResponse(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'run_id': 'int',
        'file_name': 'str',
        'application_count': 'int'
    }

    attribute_map = {
        'run_id': 'runId',
        'file_name': 'fileName',
        'application_count': 'applicationCount'
    }

    def __init__(self, run_id=None, file_name='', application_count=None):  # noqa: E501
        """RunRulesetResponse - a model defined in Swagger"""  # noqa: E501

        self._run_id = None
        self._file_name = None
        self._application_count = None
        self.discriminator = None

        if run_id is not None:
            self.run_id = run_id
        if file_name is not None:
            self.file_name = file_name
        if application_count is not None:
            self.application_count = application_count

    @property
    def run_id(self):
        """Gets the run_id of this RunRulesetResponse.  # noqa: E501


        :return: The run_id of this RunRulesetResponse.  # noqa: E501
        :rtype: int
        """
        return self._run_id

    @run_id.setter
    def run_id(self, run_id):
        """Sets the run_id of this RunRulesetResponse.


        :param run_id: The run_id of this RunRulesetResponse.  # noqa: E501
        :type: int
        """

        self._run_id = run_id

    @property
    def file_name(self):
        """Gets the file_name of this RunRulesetResponse.  # noqa: E501


        :return: The file_name of this RunRulesetResponse.  # noqa: E501
        :rtype: str
        """
        return self._file_name

    @file_name.setter
    def file_name(self, file_name):
        """Sets the file_name of this RunRulesetResponse.


        :param file_name: The file_name of this RunRulesetResponse.  # noqa: E501
        :type: str
        """

        self._file_name = file_name

    @property
    def application_count(self):
        """Gets the application_count of this RunRulesetResponse.  # noqa: E501


        :return: The application_count of this RunRulesetResponse.  # noqa: E501
        :rtype: int
        """
        return self._application_count

    @application_count.setter
    def application_count(self, application_count):
        """Sets the application_count of this RunRulesetResponse.


        :param application_count: The application_count of this RunRulesetResponse.  # noqa: E501
        :type: int
        """

        self._application_count = application_count

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, RunRulesetResponse):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
