# coding: utf-8

"""
    TruRest

    ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class CompanyAddressItem(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'address_line1': 'str',
        'address_line2': 'str',
        'address_line3': 'str',
        'address_line4': 'str',
        'address_line5': 'str',
        'address_line6': 'str',
        'address_line7': 'str',
        'address_line8': 'str',
        'zip_postcode': 'str',
        'country': 'str',
        'moving_in_date': 'date',
        'moving_out_date': 'date'
    }

    attribute_map = {
        'address_line1': 'addressLine1',
        'address_line2': 'addressLine2',
        'address_line3': 'addressLine3',
        'address_line4': 'addressLine4',
        'address_line5': 'addressLine5',
        'address_line6': 'addressLine6',
        'address_line7': 'addressLine7',
        'address_line8': 'addressLine8',
        'zip_postcode': 'zipPostcode',
        'country': 'country',
        'moving_in_date': 'movingInDate',
        'moving_out_date': 'movingOutDate'
    }

    def __init__(self, address_line1='', address_line2='', address_line3='', address_line4='', address_line5='', address_line6='', address_line7='', address_line8='', zip_postcode='', country='', moving_in_date=None, moving_out_date=None):  # noqa: E501
        """CompanyAddressItem - a model defined in Swagger"""  # noqa: E501

        self._address_line1 = None
        self._address_line2 = None
        self._address_line3 = None
        self._address_line4 = None
        self._address_line5 = None
        self._address_line6 = None
        self._address_line7 = None
        self._address_line8 = None
        self._zip_postcode = None
        self._country = None
        self._moving_in_date = None
        self._moving_out_date = None
        self.discriminator = None

        if address_line1 is not None:
            self.address_line1 = address_line1
        if address_line2 is not None:
            self.address_line2 = address_line2
        if address_line3 is not None:
            self.address_line3 = address_line3
        if address_line4 is not None:
            self.address_line4 = address_line4
        if address_line5 is not None:
            self.address_line5 = address_line5
        if address_line6 is not None:
            self.address_line6 = address_line6
        if address_line7 is not None:
            self.address_line7 = address_line7
        if address_line8 is not None:
            self.address_line8 = address_line8
        if zip_postcode is not None:
            self.zip_postcode = zip_postcode
        if country is not None:
            self.country = country
        if moving_in_date is not None:
            self.moving_in_date = moving_in_date
        if moving_out_date is not None:
            self.moving_out_date = moving_out_date

    @property
    def address_line1(self):
        """Gets the address_line1 of this CompanyAddressItem.  # noqa: E501


        :return: The address_line1 of this CompanyAddressItem.  # noqa: E501
        :rtype: str
        """
        return self._address_line1

    @address_line1.setter
    def address_line1(self, address_line1):
        """Sets the address_line1 of this CompanyAddressItem.


        :param address_line1: The address_line1 of this CompanyAddressItem.  # noqa: E501
        :type: str
        """

        self._address_line1 = address_line1

    @property
    def address_line2(self):
        """Gets the address_line2 of this CompanyAddressItem.  # noqa: E501


        :return: The address_line2 of this CompanyAddressItem.  # noqa: E501
        :rtype: str
        """
        return self._address_line2

    @address_line2.setter
    def address_line2(self, address_line2):
        """Sets the address_line2 of this CompanyAddressItem.


        :param address_line2: The address_line2 of this CompanyAddressItem.  # noqa: E501
        :type: str
        """

        self._address_line2 = address_line2

    @property
    def address_line3(self):
        """Gets the address_line3 of this CompanyAddressItem.  # noqa: E501


        :return: The address_line3 of this CompanyAddressItem.  # noqa: E501
        :rtype: str
        """
        return self._address_line3

    @address_line3.setter
    def address_line3(self, address_line3):
        """Sets the address_line3 of this CompanyAddressItem.


        :param address_line3: The address_line3 of this CompanyAddressItem.  # noqa: E501
        :type: str
        """

        self._address_line3 = address_line3

    @property
    def address_line4(self):
        """Gets the address_line4 of this CompanyAddressItem.  # noqa: E501


        :return: The address_line4 of this CompanyAddressItem.  # noqa: E501
        :rtype: str
        """
        return self._address_line4

    @address_line4.setter
    def address_line4(self, address_line4):
        """Sets the address_line4 of this CompanyAddressItem.


        :param address_line4: The address_line4 of this CompanyAddressItem.  # noqa: E501
        :type: str
        """

        self._address_line4 = address_line4

    @property
    def address_line5(self):
        """Gets the address_line5 of this CompanyAddressItem.  # noqa: E501


        :return: The address_line5 of this CompanyAddressItem.  # noqa: E501
        :rtype: str
        """
        return self._address_line5

    @address_line5.setter
    def address_line5(self, address_line5):
        """Sets the address_line5 of this CompanyAddressItem.


        :param address_line5: The address_line5 of this CompanyAddressItem.  # noqa: E501
        :type: str
        """

        self._address_line5 = address_line5

    @property
    def address_line6(self):
        """Gets the address_line6 of this CompanyAddressItem.  # noqa: E501


        :return: The address_line6 of this CompanyAddressItem.  # noqa: E501
        :rtype: str
        """
        return self._address_line6

    @address_line6.setter
    def address_line6(self, address_line6):
        """Sets the address_line6 of this CompanyAddressItem.


        :param address_line6: The address_line6 of this CompanyAddressItem.  # noqa: E501
        :type: str
        """

        self._address_line6 = address_line6

    @property
    def address_line7(self):
        """Gets the address_line7 of this CompanyAddressItem.  # noqa: E501


        :return: The address_line7 of this CompanyAddressItem.  # noqa: E501
        :rtype: str
        """
        return self._address_line7

    @address_line7.setter
    def address_line7(self, address_line7):
        """Sets the address_line7 of this CompanyAddressItem.


        :param address_line7: The address_line7 of this CompanyAddressItem.  # noqa: E501
        :type: str
        """

        self._address_line7 = address_line7

    @property
    def address_line8(self):
        """Gets the address_line8 of this CompanyAddressItem.  # noqa: E501


        :return: The address_line8 of this CompanyAddressItem.  # noqa: E501
        :rtype: str
        """
        return self._address_line8

    @address_line8.setter
    def address_line8(self, address_line8):
        """Sets the address_line8 of this CompanyAddressItem.


        :param address_line8: The address_line8 of this CompanyAddressItem.  # noqa: E501
        :type: str
        """

        self._address_line8 = address_line8

    @property
    def zip_postcode(self):
        """Gets the zip_postcode of this CompanyAddressItem.  # noqa: E501


        :return: The zip_postcode of this CompanyAddressItem.  # noqa: E501
        :rtype: str
        """
        return self._zip_postcode

    @zip_postcode.setter
    def zip_postcode(self, zip_postcode):
        """Sets the zip_postcode of this CompanyAddressItem.


        :param zip_postcode: The zip_postcode of this CompanyAddressItem.  # noqa: E501
        :type: str
        """

        self._zip_postcode = zip_postcode

    @property
    def country(self):
        """Gets the country of this CompanyAddressItem.  # noqa: E501


        :return: The country of this CompanyAddressItem.  # noqa: E501
        :rtype: str
        """
        return self._country

    @country.setter
    def country(self, country):
        """Sets the country of this CompanyAddressItem.


        :param country: The country of this CompanyAddressItem.  # noqa: E501
        :type: str
        """

        self._country = country

    @property
    def moving_in_date(self):
        """Gets the moving_in_date of this CompanyAddressItem.  # noqa: E501


        :return: The moving_in_date of this CompanyAddressItem.  # noqa: E501
        :rtype: date
        """
        return self._moving_in_date

    @moving_in_date.setter
    def moving_in_date(self, moving_in_date):
        """Sets the moving_in_date of this CompanyAddressItem.


        :param moving_in_date: The moving_in_date of this CompanyAddressItem.  # noqa: E501
        :type: date
        """

        self._moving_in_date = moving_in_date

    @property
    def moving_out_date(self):
        """Gets the moving_out_date of this CompanyAddressItem.  # noqa: E501


        :return: The moving_out_date of this CompanyAddressItem.  # noqa: E501
        :rtype: date
        """
        return self._moving_out_date

    @moving_out_date.setter
    def moving_out_date(self, moving_out_date):
        """Sets the moving_out_date of this CompanyAddressItem.


        :param moving_out_date: The moving_out_date of this CompanyAddressItem.  # noqa: E501
        :type: date
        """

        self._moving_out_date = moving_out_date

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, CompanyAddressItem):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
