# coding: utf-8

"""
    TruRest

    ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from trunarrative_client.models.image import Image  # noqa: F401,E501


class DocumentImage(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'type': 'str',
        'pages': 'list[Image]'
    }

    attribute_map = {
        'type': 'Type',
        'pages': 'Pages'
    }

    def __init__(self, type='', pages=None):  # noqa: E501
        """DocumentImage - a model defined in Swagger"""  # noqa: E501

        self._type = None
        self._pages = None
        self.discriminator = None

        if type is not None:
            self.type = type
        if pages is not None:
            self.pages = pages

    @property
    def type(self):
        """Gets the type of this DocumentImage.  # noqa: E501


        :return: The type of this DocumentImage.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this DocumentImage.


        :param type: The type of this DocumentImage.  # noqa: E501
        :type: str
        """

        self._type = type

    @property
    def pages(self):
        """Gets the pages of this DocumentImage.  # noqa: E501


        :return: The pages of this DocumentImage.  # noqa: E501
        :rtype: list[Image]
        """
        return self._pages

    @pages.setter
    def pages(self, pages):
        """Sets the pages of this DocumentImage.


        :param pages: The pages of this DocumentImage.  # noqa: E501
        :type: list[Image]
        """

        self._pages = pages

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, DocumentImage):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
