# coding: utf-8

"""
    TruRest

    ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class AppCall(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'app_id': 'int',
        'external_entity_id': 'str',
        'app_data': 'str'
    }

    attribute_map = {
        'app_id': 'appId',
        'external_entity_id': 'externalEntityId',
        'app_data': 'appData'
    }

    def __init__(self, app_id=None, external_entity_id='', app_data=''):  # noqa: E501
        """AppCall - a model defined in Swagger"""  # noqa: E501

        self._app_id = None
        self._external_entity_id = None
        self._app_data = None
        self.discriminator = None

        if app_id is not None:
            self.app_id = app_id
        if external_entity_id is not None:
            self.external_entity_id = external_entity_id
        if app_data is not None:
            self.app_data = app_data

    @property
    def app_id(self):
        """Gets the app_id of this AppCall.  # noqa: E501


        :return: The app_id of this AppCall.  # noqa: E501
        :rtype: int
        """
        return self._app_id

    @app_id.setter
    def app_id(self, app_id):
        """Sets the app_id of this AppCall.


        :param app_id: The app_id of this AppCall.  # noqa: E501
        :type: int
        """

        self._app_id = app_id

    @property
    def external_entity_id(self):
        """Gets the external_entity_id of this AppCall.  # noqa: E501


        :return: The external_entity_id of this AppCall.  # noqa: E501
        :rtype: str
        """
        return self._external_entity_id

    @external_entity_id.setter
    def external_entity_id(self, external_entity_id):
        """Sets the external_entity_id of this AppCall.


        :param external_entity_id: The external_entity_id of this AppCall.  # noqa: E501
        :type: str
        """

        self._external_entity_id = external_entity_id

    @property
    def app_data(self):
        """Gets the app_data of this AppCall.  # noqa: E501


        :return: The app_data of this AppCall.  # noqa: E501
        :rtype: str
        """
        return self._app_data

    @app_data.setter
    def app_data(self, app_data):
        """Sets the app_data of this AppCall.


        :param app_data: The app_data of this AppCall.  # noqa: E501
        :type: str
        """

        self._app_data = app_data

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, AppCall):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
