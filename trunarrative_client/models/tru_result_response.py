# coding: utf-8

"""
    TruRest

    ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from trunarrative_client.models.tru_document import TruDocument  # noqa: F401,E501
from trunarrative_client.models.tru_face import TruFace  # noqa: F401,E501


class TruResultResponse(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'reference_id': 'str',
        'processing_id': 'str',
        'authenticity': 'str',
        'processing_result': 'str',
        'completion_status': 'str',
        'face': 'TruFace',
        'documents': 'list[TruDocument]'
    }

    attribute_map = {
        'reference_id': 'ReferenceId',
        'processing_id': 'ProcessingId',
        'authenticity': 'Authenticity',
        'processing_result': 'ProcessingResult',
        'completion_status': 'CompletionStatus',
        'face': 'Face',
        'documents': 'Documents'
    }

    def __init__(self, reference_id='', processing_id='', authenticity='', processing_result='', completion_status='', face=None, documents=None):  # noqa: E501
        """TruResultResponse - a model defined in Swagger"""  # noqa: E501

        self._reference_id = None
        self._processing_id = None
        self._authenticity = None
        self._processing_result = None
        self._completion_status = None
        self._face = None
        self._documents = None
        self.discriminator = None

        if reference_id is not None:
            self.reference_id = reference_id
        if processing_id is not None:
            self.processing_id = processing_id
        if authenticity is not None:
            self.authenticity = authenticity
        if processing_result is not None:
            self.processing_result = processing_result
        if completion_status is not None:
            self.completion_status = completion_status
        if face is not None:
            self.face = face
        if documents is not None:
            self.documents = documents

    @property
    def reference_id(self):
        """Gets the reference_id of this TruResultResponse.  # noqa: E501


        :return: The reference_id of this TruResultResponse.  # noqa: E501
        :rtype: str
        """
        return self._reference_id

    @reference_id.setter
    def reference_id(self, reference_id):
        """Sets the reference_id of this TruResultResponse.


        :param reference_id: The reference_id of this TruResultResponse.  # noqa: E501
        :type: str
        """

        self._reference_id = reference_id

    @property
    def processing_id(self):
        """Gets the processing_id of this TruResultResponse.  # noqa: E501


        :return: The processing_id of this TruResultResponse.  # noqa: E501
        :rtype: str
        """
        return self._processing_id

    @processing_id.setter
    def processing_id(self, processing_id):
        """Sets the processing_id of this TruResultResponse.


        :param processing_id: The processing_id of this TruResultResponse.  # noqa: E501
        :type: str
        """

        self._processing_id = processing_id

    @property
    def authenticity(self):
        """Gets the authenticity of this TruResultResponse.  # noqa: E501


        :return: The authenticity of this TruResultResponse.  # noqa: E501
        :rtype: str
        """
        return self._authenticity

    @authenticity.setter
    def authenticity(self, authenticity):
        """Sets the authenticity of this TruResultResponse.


        :param authenticity: The authenticity of this TruResultResponse.  # noqa: E501
        :type: str
        """

        self._authenticity = authenticity

    @property
    def processing_result(self):
        """Gets the processing_result of this TruResultResponse.  # noqa: E501


        :return: The processing_result of this TruResultResponse.  # noqa: E501
        :rtype: str
        """
        return self._processing_result

    @processing_result.setter
    def processing_result(self, processing_result):
        """Sets the processing_result of this TruResultResponse.


        :param processing_result: The processing_result of this TruResultResponse.  # noqa: E501
        :type: str
        """

        self._processing_result = processing_result

    @property
    def completion_status(self):
        """Gets the completion_status of this TruResultResponse.  # noqa: E501


        :return: The completion_status of this TruResultResponse.  # noqa: E501
        :rtype: str
        """
        return self._completion_status

    @completion_status.setter
    def completion_status(self, completion_status):
        """Sets the completion_status of this TruResultResponse.


        :param completion_status: The completion_status of this TruResultResponse.  # noqa: E501
        :type: str
        """

        self._completion_status = completion_status

    @property
    def face(self):
        """Gets the face of this TruResultResponse.  # noqa: E501


        :return: The face of this TruResultResponse.  # noqa: E501
        :rtype: TruFace
        """
        return self._face

    @face.setter
    def face(self, face):
        """Sets the face of this TruResultResponse.


        :param face: The face of this TruResultResponse.  # noqa: E501
        :type: TruFace
        """

        self._face = face

    @property
    def documents(self):
        """Gets the documents of this TruResultResponse.  # noqa: E501


        :return: The documents of this TruResultResponse.  # noqa: E501
        :rtype: list[TruDocument]
        """
        return self._documents

    @documents.setter
    def documents(self, documents):
        """Sets the documents of this TruResultResponse.


        :param documents: The documents of this TruResultResponse.  # noqa: E501
        :type: list[TruDocument]
        """

        self._documents = documents

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, TruResultResponse):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
