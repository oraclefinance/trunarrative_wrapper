# coding: utf-8

"""
    TruRest

    ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class MotorDetail(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'vehicle_reg_number': 'str',
        'engine_number': 'str',
        'chassis_number': 'str',
        'make': 'str',
        'model': 'str',
        'purchase_price': 'int',
        'new_or_used': 'str',
        'year_of_manufacture': 'str'
    }

    attribute_map = {
        'vehicle_reg_number': 'vehicleRegNumber',
        'engine_number': 'engineNumber',
        'chassis_number': 'chassisNumber',
        'make': 'make',
        'model': 'model',
        'purchase_price': 'purchasePrice',
        'new_or_used': 'newOrUsed',
        'year_of_manufacture': 'yearOfManufacture'
    }

    def __init__(self, vehicle_reg_number='', engine_number='', chassis_number='', make='', model='', purchase_price=None, new_or_used='', year_of_manufacture=''):  # noqa: E501
        """MotorDetail - a model defined in Swagger"""  # noqa: E501

        self._vehicle_reg_number = None
        self._engine_number = None
        self._chassis_number = None
        self._make = None
        self._model = None
        self._purchase_price = None
        self._new_or_used = None
        self._year_of_manufacture = None
        self.discriminator = None

        if vehicle_reg_number is not None:
            self.vehicle_reg_number = vehicle_reg_number
        if engine_number is not None:
            self.engine_number = engine_number
        if chassis_number is not None:
            self.chassis_number = chassis_number
        if make is not None:
            self.make = make
        if model is not None:
            self.model = model
        if purchase_price is not None:
            self.purchase_price = purchase_price
        if new_or_used is not None:
            self.new_or_used = new_or_used
        if year_of_manufacture is not None:
            self.year_of_manufacture = year_of_manufacture

    @property
    def vehicle_reg_number(self):
        """Gets the vehicle_reg_number of this MotorDetail.  # noqa: E501


        :return: The vehicle_reg_number of this MotorDetail.  # noqa: E501
        :rtype: str
        """
        return self._vehicle_reg_number

    @vehicle_reg_number.setter
    def vehicle_reg_number(self, vehicle_reg_number):
        """Sets the vehicle_reg_number of this MotorDetail.


        :param vehicle_reg_number: The vehicle_reg_number of this MotorDetail.  # noqa: E501
        :type: str
        """

        self._vehicle_reg_number = vehicle_reg_number

    @property
    def engine_number(self):
        """Gets the engine_number of this MotorDetail.  # noqa: E501


        :return: The engine_number of this MotorDetail.  # noqa: E501
        :rtype: str
        """
        return self._engine_number

    @engine_number.setter
    def engine_number(self, engine_number):
        """Sets the engine_number of this MotorDetail.


        :param engine_number: The engine_number of this MotorDetail.  # noqa: E501
        :type: str
        """

        self._engine_number = engine_number

    @property
    def chassis_number(self):
        """Gets the chassis_number of this MotorDetail.  # noqa: E501


        :return: The chassis_number of this MotorDetail.  # noqa: E501
        :rtype: str
        """
        return self._chassis_number

    @chassis_number.setter
    def chassis_number(self, chassis_number):
        """Sets the chassis_number of this MotorDetail.


        :param chassis_number: The chassis_number of this MotorDetail.  # noqa: E501
        :type: str
        """

        self._chassis_number = chassis_number

    @property
    def make(self):
        """Gets the make of this MotorDetail.  # noqa: E501


        :return: The make of this MotorDetail.  # noqa: E501
        :rtype: str
        """
        return self._make

    @make.setter
    def make(self, make):
        """Sets the make of this MotorDetail.


        :param make: The make of this MotorDetail.  # noqa: E501
        :type: str
        """

        self._make = make

    @property
    def model(self):
        """Gets the model of this MotorDetail.  # noqa: E501


        :return: The model of this MotorDetail.  # noqa: E501
        :rtype: str
        """
        return self._model

    @model.setter
    def model(self, model):
        """Sets the model of this MotorDetail.


        :param model: The model of this MotorDetail.  # noqa: E501
        :type: str
        """

        self._model = model

    @property
    def purchase_price(self):
        """Gets the purchase_price of this MotorDetail.  # noqa: E501


        :return: The purchase_price of this MotorDetail.  # noqa: E501
        :rtype: int
        """
        return self._purchase_price

    @purchase_price.setter
    def purchase_price(self, purchase_price):
        """Sets the purchase_price of this MotorDetail.


        :param purchase_price: The purchase_price of this MotorDetail.  # noqa: E501
        :type: int
        """

        self._purchase_price = purchase_price

    @property
    def new_or_used(self):
        """Gets the new_or_used of this MotorDetail.  # noqa: E501


        :return: The new_or_used of this MotorDetail.  # noqa: E501
        :rtype: str
        """
        return self._new_or_used

    @new_or_used.setter
    def new_or_used(self, new_or_used):
        """Sets the new_or_used of this MotorDetail.


        :param new_or_used: The new_or_used of this MotorDetail.  # noqa: E501
        :type: str
        """

        self._new_or_used = new_or_used

    @property
    def year_of_manufacture(self):
        """Gets the year_of_manufacture of this MotorDetail.  # noqa: E501


        :return: The year_of_manufacture of this MotorDetail.  # noqa: E501
        :rtype: str
        """
        return self._year_of_manufacture

    @year_of_manufacture.setter
    def year_of_manufacture(self, year_of_manufacture):
        """Sets the year_of_manufacture of this MotorDetail.


        :param year_of_manufacture: The year_of_manufacture of this MotorDetail.  # noqa: E501
        :type: str
        """

        self._year_of_manufacture = year_of_manufacture

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, MotorDetail):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
