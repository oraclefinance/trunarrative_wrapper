# coding: utf-8

"""
    TruRest

    ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from trunarrative_client.models.linked_item import LinkedItem  # noqa: F401,E501


class JourneyData(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'country': 'LinkedItem',
        'journey_type': 'LinkedItem',
        'name': 'str',
        'description': 'str',
        'is_parrallel': 'bool',
        'obsolete_rule_set': 'LinkedItem',
        'obsolete_descision_matrix': 'LinkedItem',
        'score_card': 'LinkedItem',
        'created_on': 'datetime',
        'created_by': 'LinkedItem',
        'reference': 'str',
        'is_draft': 'bool',
        'id': 'int',
        'icon': 'str',
        'app_ids': 'list[int]',
        'organisation_id': 'int',
        'sub_category_id': 'int',
        'matrix_ids': 'list[int]',
        'ruleset_ids': 'list[int]'
    }

    attribute_map = {
        'country': 'Country',
        'journey_type': 'JourneyType',
        'name': 'Name',
        'description': 'Description',
        'is_parrallel': 'IsParrallel',
        'obsolete_rule_set': 'OBSOLETE_RuleSet',
        'obsolete_descision_matrix': 'OBSOLETE_DescisionMatrix',
        'score_card': 'ScoreCard',
        'created_on': 'CreatedOn',
        'created_by': 'CreatedBy',
        'reference': 'Reference',
        'is_draft': 'IsDraft',
        'id': 'Id',
        'icon': 'Icon',
        'app_ids': 'AppIds',
        'organisation_id': 'OrganisationId',
        'sub_category_id': 'SubCategoryId',
        'matrix_ids': 'MatrixIds',
        'ruleset_ids': 'RulesetIds'
    }

    def __init__(self, country=None, journey_type=None, name='', description='', is_parrallel=None, obsolete_rule_set=None, obsolete_descision_matrix=None, score_card=None, created_on=None, created_by=None, reference='', is_draft=None, id=None, icon='', app_ids=None, organisation_id=None, sub_category_id=None, matrix_ids=None, ruleset_ids=None):  # noqa: E501
        """JourneyData - a model defined in Swagger"""  # noqa: E501

        self._country = None
        self._journey_type = None
        self._name = None
        self._description = None
        self._is_parrallel = None
        self._obsolete_rule_set = None
        self._obsolete_descision_matrix = None
        self._score_card = None
        self._created_on = None
        self._created_by = None
        self._reference = None
        self._is_draft = None
        self._id = None
        self._icon = None
        self._app_ids = None
        self._organisation_id = None
        self._sub_category_id = None
        self._matrix_ids = None
        self._ruleset_ids = None
        self.discriminator = None

        self.country = country
        self.journey_type = journey_type
        self.name = name
        if description is not None:
            self.description = description
        if is_parrallel is not None:
            self.is_parrallel = is_parrallel
        if obsolete_rule_set is not None:
            self.obsolete_rule_set = obsolete_rule_set
        if obsolete_descision_matrix is not None:
            self.obsolete_descision_matrix = obsolete_descision_matrix
        if score_card is not None:
            self.score_card = score_card
        if created_on is not None:
            self.created_on = created_on
        if created_by is not None:
            self.created_by = created_by
        if reference is not None:
            self.reference = reference
        if is_draft is not None:
            self.is_draft = is_draft
        self.id = id
        if icon is not None:
            self.icon = icon
        if app_ids is not None:
            self.app_ids = app_ids
        self.organisation_id = organisation_id
        if sub_category_id is not None:
            self.sub_category_id = sub_category_id
        if matrix_ids is not None:
            self.matrix_ids = matrix_ids
        if ruleset_ids is not None:
            self.ruleset_ids = ruleset_ids

    @property
    def country(self):
        """Gets the country of this JourneyData.  # noqa: E501


        :return: The country of this JourneyData.  # noqa: E501
        :rtype: LinkedItem
        """
        return self._country

    @country.setter
    def country(self, country):
        """Sets the country of this JourneyData.


        :param country: The country of this JourneyData.  # noqa: E501
        :type: LinkedItem
        """
        if country is None:
            raise ValueError("Invalid value for `country`, must not be `None`")  # noqa: E501

        self._country = country

    @property
    def journey_type(self):
        """Gets the journey_type of this JourneyData.  # noqa: E501


        :return: The journey_type of this JourneyData.  # noqa: E501
        :rtype: LinkedItem
        """
        return self._journey_type

    @journey_type.setter
    def journey_type(self, journey_type):
        """Sets the journey_type of this JourneyData.


        :param journey_type: The journey_type of this JourneyData.  # noqa: E501
        :type: LinkedItem
        """
        if journey_type is None:
            raise ValueError("Invalid value for `journey_type`, must not be `None`")  # noqa: E501

        self._journey_type = journey_type

    @property
    def name(self):
        """Gets the name of this JourneyData.  # noqa: E501


        :return: The name of this JourneyData.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this JourneyData.


        :param name: The name of this JourneyData.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def description(self):
        """Gets the description of this JourneyData.  # noqa: E501


        :return: The description of this JourneyData.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this JourneyData.


        :param description: The description of this JourneyData.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def is_parrallel(self):
        """Gets the is_parrallel of this JourneyData.  # noqa: E501


        :return: The is_parrallel of this JourneyData.  # noqa: E501
        :rtype: bool
        """
        return self._is_parrallel

    @is_parrallel.setter
    def is_parrallel(self, is_parrallel):
        """Sets the is_parrallel of this JourneyData.


        :param is_parrallel: The is_parrallel of this JourneyData.  # noqa: E501
        :type: bool
        """

        self._is_parrallel = is_parrallel

    @property
    def obsolete_rule_set(self):
        """Gets the obsolete_rule_set of this JourneyData.  # noqa: E501


        :return: The obsolete_rule_set of this JourneyData.  # noqa: E501
        :rtype: LinkedItem
        """
        return self._obsolete_rule_set

    @obsolete_rule_set.setter
    def obsolete_rule_set(self, obsolete_rule_set):
        """Sets the obsolete_rule_set of this JourneyData.


        :param obsolete_rule_set: The obsolete_rule_set of this JourneyData.  # noqa: E501
        :type: LinkedItem
        """

        self._obsolete_rule_set = obsolete_rule_set

    @property
    def obsolete_descision_matrix(self):
        """Gets the obsolete_descision_matrix of this JourneyData.  # noqa: E501


        :return: The obsolete_descision_matrix of this JourneyData.  # noqa: E501
        :rtype: LinkedItem
        """
        return self._obsolete_descision_matrix

    @obsolete_descision_matrix.setter
    def obsolete_descision_matrix(self, obsolete_descision_matrix):
        """Sets the obsolete_descision_matrix of this JourneyData.


        :param obsolete_descision_matrix: The obsolete_descision_matrix of this JourneyData.  # noqa: E501
        :type: LinkedItem
        """

        self._obsolete_descision_matrix = obsolete_descision_matrix

    @property
    def score_card(self):
        """Gets the score_card of this JourneyData.  # noqa: E501


        :return: The score_card of this JourneyData.  # noqa: E501
        :rtype: LinkedItem
        """
        return self._score_card

    @score_card.setter
    def score_card(self, score_card):
        """Sets the score_card of this JourneyData.


        :param score_card: The score_card of this JourneyData.  # noqa: E501
        :type: LinkedItem
        """

        self._score_card = score_card

    @property
    def created_on(self):
        """Gets the created_on of this JourneyData.  # noqa: E501


        :return: The created_on of this JourneyData.  # noqa: E501
        :rtype: datetime
        """
        return self._created_on

    @created_on.setter
    def created_on(self, created_on):
        """Sets the created_on of this JourneyData.


        :param created_on: The created_on of this JourneyData.  # noqa: E501
        :type: datetime
        """

        self._created_on = created_on

    @property
    def created_by(self):
        """Gets the created_by of this JourneyData.  # noqa: E501


        :return: The created_by of this JourneyData.  # noqa: E501
        :rtype: LinkedItem
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this JourneyData.


        :param created_by: The created_by of this JourneyData.  # noqa: E501
        :type: LinkedItem
        """

        self._created_by = created_by

    @property
    def reference(self):
        """Gets the reference of this JourneyData.  # noqa: E501


        :return: The reference of this JourneyData.  # noqa: E501
        :rtype: str
        """
        return self._reference

    @reference.setter
    def reference(self, reference):
        """Sets the reference of this JourneyData.


        :param reference: The reference of this JourneyData.  # noqa: E501
        :type: str
        """

        self._reference = reference

    @property
    def is_draft(self):
        """Gets the is_draft of this JourneyData.  # noqa: E501


        :return: The is_draft of this JourneyData.  # noqa: E501
        :rtype: bool
        """
        return self._is_draft

    @is_draft.setter
    def is_draft(self, is_draft):
        """Sets the is_draft of this JourneyData.


        :param is_draft: The is_draft of this JourneyData.  # noqa: E501
        :type: bool
        """

        self._is_draft = is_draft

    @property
    def id(self):
        """Gets the id of this JourneyData.  # noqa: E501


        :return: The id of this JourneyData.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this JourneyData.


        :param id: The id of this JourneyData.  # noqa: E501
        :type: int
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def icon(self):
        """Gets the icon of this JourneyData.  # noqa: E501


        :return: The icon of this JourneyData.  # noqa: E501
        :rtype: str
        """
        return self._icon

    @icon.setter
    def icon(self, icon):
        """Sets the icon of this JourneyData.


        :param icon: The icon of this JourneyData.  # noqa: E501
        :type: str
        """

        self._icon = icon

    @property
    def app_ids(self):
        """Gets the app_ids of this JourneyData.  # noqa: E501


        :return: The app_ids of this JourneyData.  # noqa: E501
        :rtype: list[int]
        """
        return self._app_ids

    @app_ids.setter
    def app_ids(self, app_ids):
        """Sets the app_ids of this JourneyData.


        :param app_ids: The app_ids of this JourneyData.  # noqa: E501
        :type: list[int]
        """

        self._app_ids = app_ids

    @property
    def organisation_id(self):
        """Gets the organisation_id of this JourneyData.  # noqa: E501


        :return: The organisation_id of this JourneyData.  # noqa: E501
        :rtype: int
        """
        return self._organisation_id

    @organisation_id.setter
    def organisation_id(self, organisation_id):
        """Sets the organisation_id of this JourneyData.


        :param organisation_id: The organisation_id of this JourneyData.  # noqa: E501
        :type: int
        """
        if organisation_id is None:
            raise ValueError("Invalid value for `organisation_id`, must not be `None`")  # noqa: E501

        self._organisation_id = organisation_id

    @property
    def sub_category_id(self):
        """Gets the sub_category_id of this JourneyData.  # noqa: E501

        the journey sub category based on artefacts  # noqa: E501

        :return: The sub_category_id of this JourneyData.  # noqa: E501
        :rtype: int
        """
        return self._sub_category_id

    @sub_category_id.setter
    def sub_category_id(self, sub_category_id):
        """Sets the sub_category_id of this JourneyData.

        the journey sub category based on artefacts  # noqa: E501

        :param sub_category_id: The sub_category_id of this JourneyData.  # noqa: E501
        :type: int
        """

        self._sub_category_id = sub_category_id

    @property
    def matrix_ids(self):
        """Gets the matrix_ids of this JourneyData.  # noqa: E501


        :return: The matrix_ids of this JourneyData.  # noqa: E501
        :rtype: list[int]
        """
        return self._matrix_ids

    @matrix_ids.setter
    def matrix_ids(self, matrix_ids):
        """Sets the matrix_ids of this JourneyData.


        :param matrix_ids: The matrix_ids of this JourneyData.  # noqa: E501
        :type: list[int]
        """

        self._matrix_ids = matrix_ids

    @property
    def ruleset_ids(self):
        """Gets the ruleset_ids of this JourneyData.  # noqa: E501


        :return: The ruleset_ids of this JourneyData.  # noqa: E501
        :rtype: list[int]
        """
        return self._ruleset_ids

    @ruleset_ids.setter
    def ruleset_ids(self, ruleset_ids):
        """Sets the ruleset_ids of this JourneyData.


        :param ruleset_ids: The ruleset_ids of this JourneyData.  # noqa: E501
        :type: list[int]
        """

        self._ruleset_ids = ruleset_ids

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, JourneyData):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
