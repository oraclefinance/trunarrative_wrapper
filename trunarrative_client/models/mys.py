# coding: utf-8

"""
    TruRest

    ## Authentication  Basic Authentication is required for all requests.  # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from trunarrative_client.models.identity_carditem import IdentityCarditem  # noqa: F401,E501


class MYS(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'identity_card': 'list[IdentityCarditem]'
    }

    attribute_map = {
        'identity_card': 'identityCard'
    }

    def __init__(self, identity_card=None):  # noqa: E501
        """MYS - a model defined in Swagger"""  # noqa: E501

        self._identity_card = None
        self.discriminator = None

        if identity_card is not None:
            self.identity_card = identity_card

    @property
    def identity_card(self):
        """Gets the identity_card of this MYS.  # noqa: E501


        :return: The identity_card of this MYS.  # noqa: E501
        :rtype: list[IdentityCarditem]
        """
        return self._identity_card

    @identity_card.setter
    def identity_card(self, identity_card):
        """Sets the identity_card of this MYS.


        :param identity_card: The identity_card of this MYS.  # noqa: E501
        :type: list[IdentityCarditem]
        """

        self._identity_card = identity_card

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, MYS):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
